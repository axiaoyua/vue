const express = require('express')

const router = express.Router()

const fs = require('fs')

const async = require('async')
const PageModel = require('./models/pages')

const moment = require('moment')

// 请求主页的数据
router.post('/getHomeData', (req, res) => {
    let tasks = {
        getData: function (cb) {
            PageModel.find().then(data => {
                cb(null, { data })
            })
        }
    }
    async.auto(tasks, (error, endRes) => {
        if (error) {
            res.send(error)
        } else {
            res.send(endRes.getData.data)
        }
    })

})

// 请求主页热门文章
router.post('/getHotPage',(req,res)=>{
    let tasks={
        getPramas:function(cb){
            var {index,num}=req.body
            cb(null,{index,num})
        },
        getAllData:["getPramas",function(value,cb){
            
            // 排序sort,限制limit,略过skip
            // 先判断所有文章长度
            PageModel.find({}).then(res=>{
                // res:所有的文章数据
                cb(null,{res})
                
            })
        }],
        getLimitData:["getAllData",function(value,cb){
            var {index,num}=value.getPramas
            var {res}=value.getAllData
            var bool
            if(index==0){
                if(res.length > num){
                    bool=true
                    PageModel.find({}).sort({count:-1}).limit(num).then(res1=>{
                        cb(null,{res1,bool})
                        // true
                    })
                }else{
                    bool=false
                    PageModel.find({}).sort({count:-1}).then(res1=>{
                        // false
                        cb(null,{res1,bool})
                    })
                }
            }else{
                if(res.length - num*index >num ){
                    bool=true
                    PageModel.find({}).sort({count:-1}).skip(num*index).limit(num).then(res1=>{
                        cb(null,{res1,bool})
                        // true
                    })
                }else{
                    bool=false
                    PageModel.find({}).sort({count:-1}).skip(num*index).then(res1=>{
                        cb(null,{res1,bool})
                        // false
                    })
                }
            }
            
        }]
    }
    async.auto(tasks,(error,endRes)=>{
        if(error){
            res.send(error)
        }else{
            // 如果文章有18篇,index从0开始,那么index=3时,endRes.getLimitData.bool应该是false
            // console.log(endRes.getLimitData.res1,endRes.getLimitData.bool);
            // res.send('qqqqqqq')
            var arr=endRes.getLimitData.res1
            var boolen=endRes.getLimitData.bool
            res.send({arr,boolen})
        }
    })
})

// 请求分类下的全部文章
router.post('/getClassifyList', (req, res) => {
    let tasks = {
        getPramas: function (cb) {
            var { classify } = req.body
            cb(null, { classify })
        },
        getClassifyPage: ['getPramas', function (value, cb) {
            var { classify } = value.getPramas
            PageModel.find({ classify: `${classify}` }).then(res => {
                cb(null, { res })
            })

        }]
    }
    async.auto(tasks, (error, endRes) => {
        if (error) {
            res.send(error)
        } else {
            res.send(endRes.getClassifyPage.res)
        }
    })
})


// 赞接口,踩接口,点击次数接口
router.post('/AddGood', (req, res) => {
    let tasks = {
        getPramas: function (cb) {
            var { number, id, type } = req.body
            cb(null, { number, id, type })
        },
        updateData: ["getPramas", function (value, cb) {
            var { number, id, type } = value.getPramas
            if (type == "good") {
                // 根据id修改数据
                PageModel.updateOne({ _id: id }, { good: ++number }).then(res => {
                    cb(null)
                })
            }else if(type == "bad"){
                PageModel.updateOne({ _id: id }, { bad: ++number }).then(res => {
                    cb(null)
                })
            }else{
                PageModel.updateOne({ _id: id }, { count: ++number }).then(res => {
                    cb(null)
                })
            }

        }]
    }
    async.auto(tasks, (error, endRes) => {
        if (error) {
            res.send(error)
        } else {
            res.send('qqqq')
        }
    })
})

// 请求文章详情页数据
router.post('/getPageDetail',(req,res)=>{
    var tasks={
        getPramas:function(cb){
            var {id}=req.body
            cb(null,{id})
        },
        getPageData:["getPramas",function(value,cb){
            var {id}=value.getPramas
            PageModel.find({_id:id}).then(res=>{
                cb(null,{res})
            })
        }]
    }
    async.auto(tasks,(error,endRes)=>{
        if(error){
            res.send(error)
        }else{
            res.send(endRes.getPageData.res)
        }
    })
})

// 添加文章接口
router.post('/addPage',(req,res)=>{
    let tasks={
        getPramas:function(cb){
            var {title,content,classify}=req.body
            cb(null,{title,content,classify})
        },
        addPageData:["getPramas",function(value,cb){
            var {title,content,classify}=value.getPramas
            classify=classify.toLowerCase()
            let newPage=new PageModel({
                title,
                content,
                time:moment().format('MMMM Do YYYY, h:mm:ss a'),
                count:0,
                good:0,
                bad:0,
                classify
            })
            newPage.save().then(res=>{
                cb(null,res)
            })
        }]
    }
    async.auto(tasks,(error,endRes)=>{
        if(error){
            res.send(error)
        }else{
            res.send('qqqqqqq')
        }
    })
})


module.exports = router