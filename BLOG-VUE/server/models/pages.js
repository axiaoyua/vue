const mongoose = require('../db');

const Pages = mongoose.Schema({
    title : String,
    content:String,
    time : String,
    count : Number,
    good:Number,
    bad:Number,
    classify : String
})

const PageModel = mongoose.model('pages' , Pages);

const pageTest1=new PageModel({
    title:'JS1',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'js'
})

const pageTest2=new PageModel({
    title:'JS2',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'js'
})
const pageTest3=new PageModel({
    title:'node1',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'node'
})
const pageTest4=new PageModel({
    title:'node2',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'node'
})
const pageTest5=new PageModel({
    title:'http1',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'http'
})
const pageTest6=new PageModel({
    title:'http2',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'http'
})
const pageTest7=new PageModel({
    title:'ajax1',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'ajax'
})
const pageTest8=new PageModel({
    title:'ajax2',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'ajax'
})
const pageTest9=new PageModel({
    title:'react1',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'react'
})
const pageTest10=new PageModel({
    title:'react2',
    content:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita mollitia, reprehenderit odio ea in tempore illo necessitatibus voluptatibus eius, modi dicta minima, nisi debitis provident! Perspiciatis, fuga explicabo earum ratione, eum voluptatem architecto, a nisi libero tempore ipsa molestias sapiente deleniti obcaecati saepe aliquid vel voluptas maiores ex placeat asperiores odio molestiae velit. Iusto repellat culpa iure debitis nostrum porro reprehenderit, molestias est quam ad minima sed vel similique perferendis possimus sunt earum minus id libero quae, reiciendis odio? Delectus dolorem aliquam officiis. Ab accusantium dicta necessitatibus, quae magni, sequi modi quisquam commodi nostrum odio cum dolore ducimus aliquam impedit?',
    time:"March 17th 2022, 7:55:28 pm",
    count:0,
    good:0,
    bad:0,
    classify:'react'
})
// pageTest1.save()
// pageTest2.save()
// pageTest3.save()
// pageTest4.save()
// pageTest5.save()
// pageTest6.save()
// pageTest7.save()
// pageTest8.save()
// pageTest9.save()
// pageTest10.save()

module.exports = PageModel