const express =require('express')

const app =express()

// 引入cors，解决跨域
const cors=require('cors')
app.use(cors())

// 解析post请求
app.use(express.urlencoded({extended:false}))
app.use(express.json())

app.use('/',require('./router'))

app.listen(9999)