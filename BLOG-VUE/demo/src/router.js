import Vue from 'vue';
import VueRouter from 'vue-router' 

Vue.use(VueRouter)

const router=new VueRouter({
    routes:[
        {
            path:"/",
            component:()=>{return import('./components/homePage.vue')}
        },
        {
            path:'/write',
            component:()=>{return import('./components/writePage.vue')}
        },
        {
            path:'/:classify/:id',
            component:()=>{return import('./components/pageDetail.vue')}
        },
        {
            path:"/:classify",
            component:()=>{return import('./components/classifyPage.vue')}
        }
    ]
})

export default router