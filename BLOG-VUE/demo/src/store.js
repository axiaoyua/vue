import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        dataList: []
    },
    getters: {
        getNavList:function(state){
            var navList=[]
            for(var i=0;i<state.dataList.length;i++){
                navList.push(state.dataList[i].classify)
            }
            if(navList){
                navList=Array.from(new Set(navList))
            }
           return navList
            
        },
    },
    mutations: {
        changeDataList: function (state, list) {
            state.dataList = list
        }
    },
    actions: {
        requestDataList: function (context) {
            axios.post("http://localhost:9999/getHomeData").then((result) => {
                let list = result.data
                context.commit('changeDataList', list)
            })
        }
    },
    modules: {
        classify: {
            state: {
                classifyList: []
            },
            getters: {
                getClassifyList: function (state) {
                    return state.classifyList
                }
            },
            mutations: {
                changeClassifyList: function (state,list) {
                    state.classifyList=list
                }
            },
            actions: {
                requestClassifyList: function (context,value) {
                    axios.post("http://localhost:9999/getClassifyList",{classify:value}).then(res=>{
                        context.commit("changeClassifyList",res.data)
                    })
                }
            }
        },
        pageDetail:{
            state:{
                pageObj:{}
            },
            getters:{
                getPageObj:function(state){
                    return state.pageObj
                }
            },
            mutations:{
                changePageObj:function(state,value){
                    state.pageObj=value
                }
            },
            actions:{
                requestPageDetail:function(context,value){
                    axios.post("http://localhost:9999/getPageDetail",{id:value}).then(res=>{
                        context.commit("changePageObj",res.data[0])
                    })
                }
            }
        },
        hotPageList:{
            state:{
                hotList:[],
                canClick:false
            },
            getters:{
                getHotList:function(state){
                    return state.hotList
                },
                getCanClick:function(state){
                    return state.canClick
                }
            },
            mutations:{
                changeHotList:function(state,value){
                    state.hotList=value.arr
                    state.canClick=value.boolen
                },
            },
            actions:{
                requestHotPage:function(context,value){
                    var {index,num}=value
                    axios.post("http://localhost:9999/getHotPage",{index,num}).then(res=>{
                        var {arr,boolen}=res.data
                        context.commit("changeHotList",{arr,boolen})
                    })
                }
            }
        },
        writePage:{
            state:{
                writeObj:{
                    title:'',
                    content:'',
                    classify:''
                }
            },
            getters:{
                getObjTitle:function(state){
                    return state.writeObj.title
                },
                getObjContent:function(state){
                    return state.writeObj.content
                },
                getObjClassify:function(state){
                    return state.writeObj.classify
                }
            },
            mutations:{
                setTitle:function(state,value){
                    state.writeObj.title=value
                },
                setContent:function(state,value){
                    state.writeObj.content=value
                },
                setClaasify:function(state,value){
                    state.writeObj.classify=value
                },
                clearValues:function(state){
                    state.writeObj.title=""
                    state.writeObj.content=""
                    state.writeObj.classify=""
                }
            },
            actions:{
                getInputTitle:function(context,value){
                    context.commit("setTitle",value)
                },
                getInputContent:function(context,value){
                    context.commit("setContent",value)
                },
                getInputClassify:function(context,value){
                    context.commit("setClaasify",value)
                },
                submitPage:function(context,obj){
                    axios.post("http://localhost:9999/addPage", { ...obj }).then(()=>{
                        context.commit("clearValues")
                    })
                }
            }
        }
    }

})

export default store